from getpass import getpass
import subprocess
import tempfile
import configparser

import html.parser
from decouple import Config, RepositoryIni
from splinter import Browser
import os

# noinspection PyUnresolvedReferences
from ipydex import IPS  # only for  debugging and development

# maybe download: https://sites.google.com/a/chromium.org/chromedriver/downloads

d = dict()
d["loggingPrefs"] = {"browser": "ALL"}
options_for_browser = dict(driver_name="chrome", headless=False, desired_capabilities=d)


# noinspection PyUnusedLocal
def book_bbb_room(**kwargs):
    config = load_config()
    pw = get_pw()

    url = "https://selfservice.zih.tu-dresden.de/l/index.php/video_pool/form"
    b = Browser(**options_for_browser)
    b.visit(url)

    b.fill("j_username", config.get("zih_username"))
    b.fill("j_password", pw)
    b.find_by_name("_eventId_proceed").click()

    # url = "https://selfservice.zih.tu-dresden.de/l/index.php/video_pool/form"
    # b.visit(url)
    b.fill("title", "Besprechung")
    b.fill("teil", "8")
    b.fill("aphone", "5")
    b.find_by_id("setTime_3").click()
    # b.fill("setTime", "2")  # immediate booking
    b.fill("dure", "240")

    if kwargs.get("wait"):
        _ = input("Waiting for user interaction in open browser")

    b.find_by_name("submit").click()

    # the resulting page contains an html table -> parse it
    tab = b.find_by_id("bodyContent").find_by_tag("table").first
    rows = tab.find_by_tag("tr")

    bookings = {}
    booking_numbers = []
    for row in rows[1:]:
        cells = row.find_by_tag("td")
        # store rows, according to booking number (first cell)
        bn = cells[0].html
        bookings[bn] = row
        booking_numbers.append(bn)

    # get the row with the highest booking number

    booking_numbers.sort()
    last_bn = booking_numbers[-1]
    last_booking = bookings[last_bn]

    # get the  "show links" button
    btns = last_booking.find_by_tag("button")
    btns2 = [b for b in btns if "Show" in b.text]
    assert len(btns2) == 1
    btns2[0].click()

    # now a modal dialog opens which contains the links we look for

    for link in b.find_by_id("dialog-link").find_by_tag("a"):
        bbb_url = html.parser.unescape(link.html)
        print(bbb_url)

    b.quit()


def load_config():
    path = os.path.join(os.environ.get("HOME"), "autobrowser.ini")

    try:
        config = Config(RepositoryIni(path))
    except FileNotFoundError:
        tmpfname = tempfile.mktemp(".ini")

        print(f"Could not find config file at: {path}. Creating {tmpfname} and ask for data.")

        ini = configparser.ConfigParser(default_section="settings")
        ini.set("settings", "zih_username", input("ZIH username? "))
        with open(tmpfname, "w") as fp:
            ini.write(fp)
        config = Config(RepositoryIni(tmpfname))

    return config


def get_pw():

    print("Try to load ZIH PW from passwordstore ...")
    try:
        res = subprocess.run(["pass", "show", "rst/zihpw"], capture_output=True)
    except FileNotFoundError:
        returncode = 1000
        res = None
    else:
        returncode = res.returncode

    if returncode == 0:
        pw = res.stdout.decode("utf8").strip()
    else:
        print("failed")
        pw = getpass("ZIH password? ")

    return pw
