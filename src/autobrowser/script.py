"""
Command line interface for autobrowser package
"""

import argparse
from ipydex import IPS, activate_ips_on_exception
from . import core

activate_ips_on_exception()


def main():

    mapping = {
        "bbb": "book bbb room",
        "pw": "test password retrieval",
        "config": "test loading of config file",
    }

    cmd_docs = ", ".join([f"`{key}` ({value})" for key, value in mapping.items()])
    parser = argparse.ArgumentParser()
    parser.add_argument("cmd", help=f"main command: allowed values: {cmd_docs}", choices=mapping.keys(), metavar="cmd")
    parser.add_argument("--wait", help=f"wait before executing final action", action="store_true")

    args = parser.parse_args()

    if args.cmd:
        if args.cmd == "bbb":
            kwargs = {"wait": args.wait}
            core.book_bbb_room(**kwargs)
        elif args.cmd == "config":
            core.load_config()
        elif args.cmd == "pw":
            core.get_pw()
        else:
            print("unknown command")
    else:
        print("nothing to do")
